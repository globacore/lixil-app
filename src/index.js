require('./styles/index.scss');

var model = require('spooky-model');
// Setup model
model.init(require('./models/site'));

var router = require('./router-main');
var domReady = require('domready');
var eve = require('dom-events');
var windowSize = require('./utils/window-size');
var spookyElement = require('spooky-element');

const remote = window.require('electron').remote;
const App = remote.app;

function init(){
  var container = document.getElementById('spooky-container');
  var body = spookyElement(document.body);

  // Reset and corner-tap quit app
  (function(){

    // Exit if clicked top-left
    var clickCount = 0;
    body.on('click', (e)=>{
      if (e.pageX < 60 && e.pageY < 60){
        clickCount++;
        if (clickCount >= 4){
          App.quit();
        }
      } else {
        clickCount = 0;
      }
    });

  })();

  var routerInit = function(){
    this.add('home', '/', {view:require('./sections/Home/Home')});
  }

  var initialSize = windowSize();

  router.init(container, routerInit, initialSize.w, initialSize.h, true);

  // RESIZE
  var resize = function(){
    var size = windowSize();
    router.resize(size.w, size.h);
  };
  resize();
  eve.on(window, 'resize', resize);
}

domReady(function(){

  init();

});
