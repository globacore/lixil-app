require('./Home.scss');
var SiteButton = require('../../ui/SiteButton/SiteButton');
var WebviewPopup = require('../../ui/WebviewPopup/WebviewPopup');

var SpookyEl = require('spooky-element');
var yo = require('yo-yo');

class Home extends SpookyEl {

  constructor(data){
    super();
    var buttonsData = [
      {
        image: require('../../images/dxv-background.png'),
        id:'dxv',
        url:'http://www.dxv.com',
        website: require('../../images/dxv-link.png')
      },
      {
        image: require('../../images/american-standard-background.png'),
        id:'as',
        url:'http://www.americanstandard-us.com',
        website: require('../../images/as-link.png')
      },
      {
        image: require('../../images/grohe-background.png'),
        id:'gr',
        url:'http://www.grohe.com',
        website: require('../../images/gr-link.png')
      }
    ];

    this.buttons = [];

    this.view = yo`<div class="section-home">
      <div class="header">
        <img src="${require('../../images/lixil-logo.png')}" alt="" />
      </div>
      <div class="buttons-container">
        ${buttonsData.map((data) => {
          var but = new SiteButton(data);
          but.on('click', (e) => {
            this.openPopup(data);
          });
          return but.view;
        })}
      </div>
    </div>`;
  }

  openPopup(data){

    if (this.popup && this.popup.view){
      this.popup.remove();
    }

    this.popup = new WebviewPopup(data);
    this.popup.appendTo(this);

  }

}

module.exports = Home;
