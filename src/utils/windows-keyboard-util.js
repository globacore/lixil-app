const remote = window.require('electron').remote;

var isWin = /^win/.test(remote.process.platform);

module.exports = {

  show: function(){
    if (isWin){
      const spawn = remote.require('child_process').spawn;
      const bat = spawn('cmd.exe', ['/c', "C:\\Program Files\\Common Files\\microsoft shared\\ink\\TabTip.exe"]);

      bat.stdout.on('data', (data) => {
        console.log(data);
      });

      bat.stderr.on('data', (data) => {
        console.log(data);
      });

      bat.on('exit', (code) => {
        console.log('Child exited with code: ', code);
      });
    }
    // var file:File = new File("C:\\Windows\\System32\\cmd.exe");
            // var processArgs:Vector.<String> = new Vector.<String>();
            // processArgs.push("/c");                   //Argument to close cmd after executing
            // processArgs.push("C:\\Program Files\\Common Files\\microsoft shared\\ink\\TabTip.exe");

  }

}
