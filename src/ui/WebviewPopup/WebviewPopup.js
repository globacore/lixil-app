require('./WebviewPopup.scss');

var SpookyEl = require('spooky-element');
var yo = require('yo-yo');
var remote = window.require('electron').remote;
var app = remote.app;
var windowsKeyboardUtil = require('../../utils/windows-keyboard-util');

class WebviewPopup extends SpookyEl {

  constructor(data){

    var appPath = app.getAppPath();
    console.log('appPath:', appPath);

    var el = yo`
    <div class="ui-webview-popup">
      <div class="webview-bg">
        <div class="webview-header">
          <img src="${require('../../images/lixil-logo2.png')}" alt="" />
        </div>
      </div>
      <webview class="webview" src="${data.url}" preload="file:${appPath+'/preload.js'}" minwidth="1800" minheight="970" disablewebsecurity></webview>
      <div class="spinner"></div>
      <div class="close-but">
        <img src="${require('../../images/close.png')}" alt="" />
      </div>
    </div>`;
    super(el, data);

    this.webviewLoader = this.find('.spinner');
    gsap.set(this.webviewLoader, {autoAlpha:0});

    // remote.ipcMain.on('showKeyboard', function(data){
    //   console.log('SHOW KEYBOARD');
    // });

    this.webview = this.find('.webview');
    this.webview.on("did-start-loading", ()=>{
      gsap.to( this.webviewLoader, 0.1, {autoAlpha:1, ease:Linear.easeNone} );
    });
    this.webview.on("did-stop-loading", ()=>{
      gsap.to( this.webviewLoader, 0.3, {autoAlpha:0, ease:Linear.easeNone} );
    });
    this.webview.on("dom-ready", ()=>{
      console.log('dom-ready');
      var contents = this.webview.view.getWebContents();
      //contents.openDevTools();
    });
    this.webview.on('ipc-message', (event) => {
      console.log('event:', event);
      if (event && event.channel == 'focus'){
        windowsKeyboardUtil.show();
      }
    });

    this.closebutton = this.find('.close-but');
    this.closebutton.on('click', (e) => {
      this.hide();
    });

    gsap.to(this, 0, {autoAlpha:0, ease:Linear.easeNone});

    this.show();

  }

  show(){
    gsap.to(this, 0.5, {autoAlpha:1, zIndex:5, ease:Linear.easeNone});
  }

  hide(){
    gsap.to(this, 0.5, {autoAlpha:0, ease:Linear.easeNone, onComplete:(arg) => {
      this.remove();
    }});
  }

}

module.exports = WebviewPopup;
