require('./SiteButton.scss');

var SpookyEl = require('spooky-element');
var yo = require('yo-yo');

class SiteButton extends SpookyEl {

  constructor(data){
    console.log(data.image);
    var el = yo`
    <img class="ui-site-button" src="${data.image}" />`;
    super(el, data);
  }

}

module.exports = SiteButton;
