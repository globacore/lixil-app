'use strict';

import baseConfig from './base';

let config = {
  env: 'dist'
};

module.exports = Object.assign({}, baseConfig, config);
