'use strict';

import baseConfig from './base';

let config = {
  env: 'test'
};

module.exports = Object.assign({}, baseConfig, config);
