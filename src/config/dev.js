'use strict';

import baseConfig from './base';

let config = {
  env: 'dev'
};

module.exports = Object.assign({}, baseConfig, config);
