const {ipcRenderer} = require('electron');
//var $ = require('jquery');

// injection technique borrowed from http://stackoverflow.com/questions/840240/injecting-jquery-into-a-page-fails-when-using-google-ajax-libraries-api
window.onload = function() {
    var script = document.createElement("script");
    script.src = "https://code.jquery.com/jquery-2.1.4.min.js";
    script.onload = script.onreadystatechange = function() {
      $(document).ready(function() {
        $("input").on('focus', function(){
          console.log('focus');
          ipcRenderer.sendToHost('focus');
        });
      });
    };
    document.body.appendChild(script);
};

ipcRenderer.on('ping', () => {
  ipcRenderer.sendToHost('pong')
})
ipcRenderer.sendToHost('pong');
// $(function(){
//   document.addEventListener('click', function(event) {
//     ipc.send('showKeyboard', event);
//   });
// });

