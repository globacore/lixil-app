'use strict';
var app = require('electron').app;
var BrowserWindow = require('electron').BrowserWindow;

// adds debug features like hotkeys for triggering dev tools and reload
require('electron-debug')({showDevTools:true});
var indexFile = `${__dirname}/index.html`;

if (process.env['NODE_ENV'] == 'dev') {
  indexFile = "http://127.0.0.1:8090";
}

// prevent window being garbage collected
let mainWindow;

function onClosed() {
  // dereference the window
  // for multiple windows store them in an array
  mainWindow = null;
}

function createMainWindow() {
  const win = new BrowserWindow({
    width: 1920,
    height: 1080,
    useContentSize: false,
    kiosk: false,
    resizable: false,
    frame: false
  });

  if (process.env['NODE_ENV'] == 'dev') {
    // we need to wait until browsersync is ready
    setTimeout(function() {
      win.loadURL(indexFile);
    }, 5000);
  } else {
    win.loadURL(`file:${indexFile}`);
  }


  win.on('closed', onClosed);

  return win;
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate-with-no-open-windows', () => {
  if (!mainWindow) {
    mainWindow = createMainWindow();
  }
});

app.on('ready', () => {
  mainWindow = createMainWindow();
});
