'use strict';
var app = require('electron').app;
var BrowserWindow = require('electron').BrowserWindow;

// adds debug features like hotkeys for triggering dev tools and reload
require('electron-debug')({showDevTools:false});
var indexFile = `${__dirname}/index.html`;

// prevent window being garbage collected
let mainWindow;

function onClosed() {
  // dereference the window
  // for multiple windows store them in an array
  mainWindow = null;
}

function createMainWindow() {
  const win = new BrowserWindow({
    width: 1920,
    height: 1080,
    useContentSize: false,
    kiosk: false,
    resizable: false,
    fullscreen: true,
    frame: false
  });

  win.loadURL(`file:${indexFile}`);

  win.on('closed', onClosed);

  return win;
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate-with-no-open-windows', () => {
  if (!mainWindow) {
    mainWindow = createMainWindow();
  }
});

app.on('ready', () => {
  mainWindow = createMainWindow();
});
